# PixivRobot </br>
nodejs练习项目 - pixiv爬虫（功能待补充） </br>


1.下载本程序zip包并解压 </br>
2.nodejs.org下载node并配置环境变量 </br>
3.命令行运行npm install </br>
4.npm start </br>
5.打开浏览器输入localhost:3000 </br>


功能： </br>
支持作品页下载 </br>
支持作者ID下载 </br>
支持浏览器预览并选择下载内容 </br>
*自动解析页面和相册 </br>


缺陷： </br>
超时重下（未完善） </br>
下载路径（未完善） </br>
下载完成提示（待补充） </br>
